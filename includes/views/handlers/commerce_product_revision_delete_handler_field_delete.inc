<?php

/**
 * @file 
 * Attribute handler for the attribute field.
 */

/**
 * Field handler to present an product revision's operation link.
 */
class commerce_product_revision_delete_handler_field_delete  extends commerce_product_handler_field_product_link {
  function construct() {
    parent::construct();

    $this->additional_fields['revision_id'] = 'revision_id';
  }

  function render($values) {
    // Ensure the user has access to delete this product.
    $product_id = $this->get_value($values, 'product_id');
    $product = commerce_product_load($product_id);

    if (!commerce_product_access('delete', $product)) {
      return;
    }

    $revision_id = $this->get_value($values, 'revision_id');
    $text = !empty($this->options['text']) ? $this->options['text'] : t('delete');

    return l($text, 'admin/commerce/products/' . $product_id . '/revisions/'. $revision_id . '/delete', array('query' => drupal_get_destination()));
  }
}
