<?php

/**
 * @file
 * Lists module's rules' actions and conditions.
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_product_revision_delete_rules_action_info() {
  $actions = [];

  $actions['commerce_product_revision_delete_delete_latest'] = [
    'label' => t('Deletes latest N revisions of the product'),
    'parameter' => [
      'commerce-product' => [
        'type' => 'commerce_product',
        'label' => t('Product'),
        'skip save' => TRUE,
      ],
      'num_to_delete' => [
        'type' => 'integer',
        'label' => t('Number of revisions to delete'),
        'description' => t('Enter the number of latest product revisions to be deleted.'),
      ],
    ],
    'group' => t('Commerce Product'),
    'callbacks' => [
      'execute' => 'commerce_product_revision_delete_delete_latest_action',
    ],
  ];

  return $actions;
}

function commerce_product_revision_delete_rules_condition_info() {
  $conditions = [];

  $conditions['commerce_product_revisions_number_evaluation'] = [
    'label' => t('Compares the number of product revisions with given value'),
    'parameter' => [
      'commerce-product' => [
        'label' => t('Product'),
        'type' => 'commerce_product',
        'description' => t('Product to evaluate the number of revisions.'),
      ],
      'operator' => [
        'label' => t('Operator'),
        'type' => 'text',
        'description' => t('The operator used with the value below to compare against the number of product revisions.'),
        'default value' => '>=',
        'options list' => 'commerce_numeric_comparison_operator_options_list',
        'restriction' => 'input',
      ],
      'value' => [
        'label' => t('Quantity'),
        'type' => 'integer',
        'default value' => '1',
        'description' => t('The value to compare against the number of product revisions.'),
      ],
    ],
    'group' => t('Commerce Product'),
    'callbacks' => [
      'execute' => 'commerce_product_revision_delete_compare_product_revisions_number',
    ],
  ];

  return $conditions;
}

/**
 * @param object $product
 * @param int $num_to_delete
 * @param array $settings
 * @param RulesState $state
 * @param RulesPlugin $element
 */
function commerce_product_revision_delete_delete_latest_action($product, $num_to_delete, $settings, RulesState $state, RulesPlugin $element) {
  $revisions_ids = commerce_product_revision_delete_get_product_revisions($product->product_id, time() + 1, 0);
  $revisions_ids = array_reverse($revisions_ids);
  array_splice($revisions_ids, $num_to_delete);

  foreach ($revisions_ids as $revision_id) {
    commerce_product_revision_delete_do_delete($revision_id);
  }
}

function commerce_product_revision_delete_compare_product_revisions_number($product, $operator, $value) {
  $num_revisions = !empty($product) ? commerce_product_revision_delete_get_num_product_revisions($product->product_id) : 0;

  switch ($operator) {
    case '<':
      return $num_revisions < $value;
    case '<=':
      return $num_revisions <= $value;
    case '=':
      return $num_revisions == $value;
    case '>=':
      return $num_revisions >= $value;
    case '>':
      return $num_revisions > $value;
  }

  return FALSE;
}