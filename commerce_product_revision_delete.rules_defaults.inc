<?php

/**
 * @file
 * Lists module's rules.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_product_revision_delete_default_rules_configuration() {
  $rules = [];

  $rule = rules_reaction_rule();
  $rule->label = t('Deletes several latest revisions on product update');
  $rule->active = FALSE;
  $rule->event('commerce_product_update')
    ->condition('commerce_product_revisions_number_evaluation', [
      'commerce-product:select' => 'commerce-product',
      'operator' => '>=',
      'value' => '500',
    ])
    ->action('commerce_product_revision_delete_delete_latest', [
      'commerce-product:select' => 'commerce-product',
      'num_to_delete' => '20',
    ]);
  $rules['commerce_product_revision_delete_latest_on_product_update'] = $rule;

  return $rules;
}
