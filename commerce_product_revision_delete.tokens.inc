<?php

/**
 * @file
 * Builds placeholder replacement tokens for product-revision-related data.
 */


/**
 * Implements hook_token_info_alter().
 */
function commerce_product_revision_delete_token_info_alter(&$data) {
  if (isset($data['tokens']['commerce-product'])) {
    $data['tokens']['commerce-product']['num-revisions'] = [
      'name' => t('Number of revisions'),
      'description' => t('The number of revisions this product has.'),
    ];
  }
}

/**
 * Implements hook_tokens().
 */
function commerce_product_revision_delete_tokens($type, $tokens, array $data = [], array $options = []) {
  $replacements = [];

  if ($type === 'commerce-product' && !empty($data['commerce-product']) && isset($tokens['num-revisions'])) {
    $product = $data['commerce-product'];
    $original = $tokens['num-revisions'];
    $replacements[$original] = commerce_product_revision_delete_get_num_product_revisions($product->product_id);
  }

  return $replacements;
}
