<?php

/**
 * @file
 * Define the view field as a part of the commerce line item. 
 *
 */

/**
 * Alter the views data to enable some additional features in views
 */
function commerce_product_revision_delete_views_data_alter(&$data) {
  foreach ($data as $table_name => &$table) {
    if ($table_name === 'commerce_product_revision') {
      $table['revision_delete'] = [
        'field' => [
          'title' => t('Delete link'),
          'help' => t('Provide a simple link to delete product revision.'),
          'handler' => 'commerce_product_revision_delete_handler_field_delete',
        ],
      ];
    }
  }
}
